# -------------------------------------------------------------------------
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.
# --------------------------------------------------------------------------

import os
from random import sample
from flask import Flask, render_template, request, redirect, url_for, send_from_directory

from webapp.helper import TableServiceHelper

app = Flask(__name__, static_folder="../static", template_folder="../templates")

@app.route("/")
def index():
    entity_list = TableServiceHelper().query_entity(request.args)
    result = TableServiceHelper.serializer(entity_list)
    result["args"] = request.args
    return render_template('index.html', **result)

@app.route("/path", methods=["GET", "POST"])
def test():
    filename = os.path.join(app.root_path, 'data/', 'sample_data.json')
    return filename

@app.route("/api/entity", methods=["POST"])
def handler_entity_action():
    action = request.args.get("action")
    sample_file = os.path.join(app.root_path, 'data/', 'sample_data.json')
    if action == "delete":
        TableServiceHelper().delete_entity()
    elif action in ["insert", "insertCustom"]:
        TableServiceHelper().insert_entity()
    elif action in ["upsert", "upsetCustom"]:
        TableServiceHelper().upsert_entity()
    elif action == "update":
        TableServiceHelper().update_entity()
    elif action == "insertSampleData":
        TableServiceHelper().insert_sample_data(sample_file)
    return redirect(url_for("index"))
