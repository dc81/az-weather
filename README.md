# Flask Azure Cosmos DB Demonstration

## Setup Environment

```
touch .env
```

## Add Environment

Add into ```.env```

## Retrieve Azure Cosmos Database Connection String

Azure Cosmos DB > Connection String

```
conn_str="XXXX"
table_name="WeatherData"
```

## Development

### Setup Environment

```
python3 -m venv .venv
```

### Activate Environment

```
source .venv/bin/activate
```

### Install Requirement

```
pip3 install -r requirements.txt
```

### Run Web Application

```
python3 run.py webapp
```

## Production

### Setup Repository

```
git clone https://gitlab.com/dc81/az-weather.git
cd az-weather

```

### Deploy Azure Web Application 

```
az webapp up --runtime PYTHON:3.9 --sku B1 --name joe111111 --location eastus -g c8 --logs
```

### Setup Environment in Azure Webapp

App Service -> Configuration

```
conn_str="XXXX"
table_name="WeatherData"
```

General Setting -> Startup Command

```
gunicorn --bind=0.0.0.0 --timeout 600 wsgi
```

App Service -> Restart -> Browse